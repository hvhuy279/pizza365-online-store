const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const drinkSchema = new Schema({
  maNuocUong: {
    type: String,
    unique: true,
    required: true,
  },
  tenNuocUong: {
    type: String,
    required: true,
  },
  donGia: {
    type: Number,
    required: true,
  },
});

module.exports = mongoose.model("Drink", drinkSchema);
