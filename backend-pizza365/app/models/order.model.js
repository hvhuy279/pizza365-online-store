const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const orderSchema = new Schema({
  orderCode: {
    type: String,
    default: function () {
      return Math.random().toString(36).substring(2, 7);
    },
    unique: true,
  },
  pizzaSize: {
    type: String,
    required: true,
  },
  pizzaType: {
    type: String,
    required: true,
  },
  voucher: {
    type: mongoose.Types.ObjectId,
    ref: "Voucher",
  },
  drink: {
    type: mongoose.Types.ObjectId,
    ref: "Drink",
  },
  status: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("Order", orderSchema);
