const express = require("express");

// Khai báo Router Middleware
const router = express.Router();

// Khai báo Middlewares
const {
  getAllDrinksMiddleware,
  getDrinkByIdMiddleware,
  createDrinkMiddleware,
  updateDrinkByIdMiddleware,
  deleteDrinkByIdMiddleware,
} = require("../middlewares/drink.middleware");

// Khai báo Controllers
const {
  getAllDrinks,
  createDrinks,
  getDrinkById,
  updateDrinkById,
  deleteDrinkById,
} = require("../controllers/drink.controller");

router.get("/", getAllDrinksMiddleware, getAllDrinks);

router.get("/:drinkid", getDrinkByIdMiddleware, getDrinkById);

router.post("/", createDrinkMiddleware, createDrinks);

router.put("/:drinkid", updateDrinkByIdMiddleware, updateDrinkById);

router.delete("/:drinkid", deleteDrinkByIdMiddleware, deleteDrinkById);

module.exports = router;
