const express = require("express");
// Khai báo Router
const router = express.Router();

// Import Middlewares
const orderMiddleware = require("../middlewares/order.middleware");

// Import Controllers
const orderController = require("../controllers/order.controller");

router.get("/", orderMiddleware.getAllOrders, orderController.getAllOrders);
router.get(
  "/:orderid",
  orderMiddleware.getOrderById,
  orderController.getOrderById
);
router.post("/", orderMiddleware.createOrder, orderController.createOrder);

router.put(
  "/:orderid",
  orderMiddleware.updateOrderById,
  orderController.updateOrderById
);
router.delete(
  "/:orderid",
  orderMiddleware.deleteOrderById,
  orderController.deleteOrderById
);

module.exports = router;
