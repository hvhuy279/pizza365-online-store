const express = require("express");
const pizzaController = require("../controllers/pizza.controller");

const route = express.Router();

route.post("/orders", pizzaController.createOrder);
route.get("/drinks", pizzaController.getDrinkList);

module.exports = route;
