// Import Express
const express = require("express");

// Import Middlewares
const {
  getAllVouchersMiddleware,
  getVoucherByIdMiddleware,
  createVoucherMiddleware,
  updateVoucherByIdMiddleware,
  deleteVoucherByIdMiddleware,
} = require("../middlewares/voucher.middleware");

// Import Controllers
const {
  getAllVouchers,
  getVoucherById,
  createVoucher,
  updateVoucher,
  deleteVoucher,
} = require("../controllers/voucher.controller");

const router = express.Router();

router.get("/", getAllVouchersMiddleware, getAllVouchers);

router.get("/:voucherid", getVoucherByIdMiddleware, getVoucherById);

router.post("/", createVoucherMiddleware, createVoucher);

router.put("/:voucherid", updateVoucherByIdMiddleware, updateVoucher);

router.delete("/:voucherid", deleteVoucherByIdMiddleware, deleteVoucher);

module.exports = router;
