const express = require("express");

// Import Middlewares
const userMiddleware = require("../middlewares/user.middleware");

// Import Controllers
const userController = require("../controllers/user.controller");

// Use Router
const router = express.Router();

// Khai bao Router
router.get(
  "/",
  userMiddleware.getAllUsersMiddleware,
  userController.getAllUsers
);

router.get("/limit-users", userController.getAllUsers);
router.get("/skip-users", userController.getSkipUsers);
router.get("/sort-users", userController.getSortAllUsers);
router.get("/skip-limit-users", userController.getSkipLimitUsers);
router.get("/sort-skip-limit-users", userController.getSortSkipLimitUsers);

router.get(
  "/:userid",
  userMiddleware.getUserByIdMiddleware,
  userController.getUserById
);

router.post(
  "/",
  userMiddleware.createUserMiddleware,
  userController.createUser
);

router.put(
  "/:userid",
  userMiddleware.updateUserByIdMiddleware,
  userController.updateUserById
);

router.delete(
  "/:userid",
  userMiddleware.deleteUserByIdMiddleware,
  userController.deleteUserById
);

module.exports = router;
