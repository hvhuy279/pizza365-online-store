const mongoose = require("mongoose");
const userModel = require("../models/user.model");
const orderModel = require("../models/order.model");

const getAllUsers = async (request, response) => {
  // B1: Thu thập dữ liệu
  const limit = request.query.limit;

  try {
    if (!limit) {
      const result = await userModel.find();

      return response.status(200).json({
        status: "Get all Users success",
        data: result,
      });
    } else {
      const result = await userModel.find().limit(limit);

      return response.status(200).json({
        status: "Get Litmit Users success",
        data: result,
      });
    }
  } catch (error) {
    return response.status(500).json({
      message: "Unexpected Error",
    });
  }
};

const getSkipUsers = async (request, response) => {
  // B1: Thu thập dữ liệu
  const skip = request.query.skip;

  try {
    if (!skip) {
      const result = await userModel.find();

      return response.status(200).json({
        status: "Get all Users success",
        data: result,
      });
    } else {
      const result = await userModel.find().skip(skip);

      return response.status(200).json({
        status: "Get Litmit Users success",
        data: result,
      });
    }
  } catch (error) {
    return response.status(500).json({
      message: "Unexpected Error",
    });
  }
};

const getSortAllUsers = async (request, response) => {
  try {
    const result = await userModel.find().sort({ fullName: "asc" });

    return response.status(200).json({
      status: "Get all Users success",
      data: result,
    });
  } catch (error) {
    return response.status(500).json({
      message: "Unexpected Error",
    });
  }
};

const getSkipLimitUsers = async (request, response) => {
  let limit = request.query.limit;
  let skip = request.query.skip;

  try {
    if (limit && skip) {
      const result = await userModel.find().skip(skip).limit(limit);

      return response.status(200).json({
        status: "Get Skip Limit Users success",
        data: result,
      });
    } else {
      const result = await userModel.find();

      return response.status(200).json({
        status: "Get all Users success",
        data: result,
      });
    }
  } catch (error) {
    return response.status(500).json({
      message: "Unexpected Error",
    });
  }
};

const getSortSkipLimitUsers = async (request, response) => {
  let limit = request.query.limit;
  let skip = request.query.skip;

  try {
    if (limit && skip) {
      const result = await userModel
        .find()
        .sort({ fullName: "asc" })
        .skip(skip)
        .limit(limit);

      return response.status(200).json({
        status: "Get Sort Skip Limit Users success",
        data: result,
      });
    } else {
      const result = await userModel.find();

      return response.status(200).json({
        status: "Get all Users success",
        data: result,
      });
    }
  } catch (error) {
    return response.status(500).json({
      message: "Unexpected Error",
    });
  }
};

const getUserById = async (request, response) => {
  // B1: Thu thập dữ liệu
  const userId = request.params.userid;

  // B2: Kiểm tra dữ liệu
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return response.status(400).json({
      message: "User ID is Invalid",
    });
  }

  // B3: Xử lý Models
  try {
    const result = await userModel.findById(userId);

    return response.status(200).json({
      status: "Get User by ID success",
      data: result,
    });
  } catch (error) {
    return response.status(500).json({
      message: "Unexpected Error",
    });
  }
};

const createUser = async (request, response) => {
  const { fullName, email, address, phone } = request.body;

  if (!fullName) {
    return response.status(400).json({
      message: "Full Name is required",
    });
  }
  if (!email) {
    return response.status(400).json({
      message: "Email is required",
    });
  }
  if (!address) {
    return response.status(400).json({
      message: "Address is required",
    });
  }
  if (!phone) {
    return response.status(400).json({
      message: "Phone is required",
    });
  }

  try {
    const newUser = {
      fullName: fullName,
      email: email,
      address: address,
      phone: phone,
    };

    const result = await userModel.create(newUser);

    return response.status(201).json({
      status: "Create User success",
      data: result,
    });
  } catch (error) {
    return response.status(500).json({
      message: "Unexpected Error",
    });
  }
};

const updateUserById = async (request, response) => {
  // B1: Thu thập dữ liệu
  const userId = request.params.userid;
  const { fullName, email, address, phone } = request.body;

  // B2: Kiểm tra dữ liệu
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return response.status(404).json({
      message: "User ID Not Found",
    });
  }
  if (!fullName) {
    return response.status(400).json({
      message: "Full Name is required",
    });
  }
  if (!email) {
    return response.status(400).json({
      message: "Email is required",
    });
  }
  if (!address) {
    return response.status(400).json({
      message: "Address is required",
    });
  }
  if (!phone) {
    return response.status(400).json({
      message: "Phone is required",
    });
  }

  // B3: Xử lý Model
  try {
    const updateUser = {
      fullName: fullName,
      email: email,
      address: address,
      phone: phone,
    };

    const result = await userModel.findByIdAndUpdate(userId, updateUser);

    return response.status(200).json({
      status: "Update User Success",
      data: result,
    });
  } catch (error) {
    return response.status(500).json({
      message: "Unexpected Error",
    });
  }
};

const deleteUserById = async (request, response) => {
  // B1: Thu thập dữ liệu
  const userId = request.params.userid;

  // B2: Kiểm tra dữ liệu
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return response.status(400).json({
      message: "User ID is Invalid",
    });
  }

  // B3: Xử lý Model
  try {
    const result = await userModel.findByIdAndDelete(userId);

    if (result) {
      return response.status(200).json({
        message: "Delete User Success",
      });
    } else {
      return response.status(404).json({
        message: "User ID Not Found",
      });
    }
  } catch (error) {
    return response.status(500).json({
      message: "Unexpected Error",
    });
  }
};

module.exports = {
  createUser,
  getAllUsers,
  getUserById,
  updateUserById,
  deleteUserById,
  getSkipUsers,
  getSortAllUsers,
  getSkipLimitUsers,
  getSortSkipLimitUsers,
};
