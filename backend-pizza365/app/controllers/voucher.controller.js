const mongoose = require("mongoose");
const voucherModel = require("../models/voucher.model");

const getAllVouchers = async (request, response) => {
  try {
    const result = await voucherModel.find();

    return response.status(200).json({
      status: "Get all Vouchers success",
      data: result,
    });
  } catch (error) {
    return response.status(500).json({
      message: "Có lỗi",
    });
  }
};

const getVoucherById = async (request, response) => {
  // B1: Thu thập dữ liệu
  const voucherId = request.params.voucherid;

  // B2: Kiểm tra dữ liệu
  if (!mongoose.Types.ObjectId.isValid(voucherId)) {
    return response.status(404).json({
      message: "Voucher ID Not Found",
    });
  }

  // B3: Xử lý Model
  try {
    const result = await voucherModel.findById(voucherId);

    return response.status(200).json({
      status: "Get Voucher By ID success",
      data: result,
    });
  } catch (error) {
    return response.status(500).json({
      message: "Có lỗi",
    });
  }
};

const createVoucher = async (request, response) => {
  // B1: Thu thập dữ liệu
  const { maVoucher, phanTramGiamGia, ghiChu } = request.body;

  // B2: Kiểm tra dữ liệu
  if (!maVoucher) {
    return response.status(400).json({
      status: "Bad Request",
      message: "Ma Voucher is required",
    });
  }
  if (phanTramGiamGia < 0 || !Number.isInteger(phanTramGiamGia)) {
    return response.status(400).json({
      status: "Bad Request",
      message: "Phan Tram Giam Gia is required",
    });
  }

  // B3: Xử lý Model
  try {
    const newVoucher = {
      maVoucher: maVoucher,
      phanTramGiamGia: phanTramGiamGia,
      ghiChu: ghiChu,
    };

    const result = await voucherModel.create(newVoucher);

    return response.status(201).json({
      status: "Create new voucher success",
      data: result,
    });
  } catch (error) {
    return response.status(500).json({
      message: "Có lỗi",
    });
  }
};

const updateVoucher = async (request, response) => {
  // B1: Thu thập dữ liệu
  const voucherId = request.params.voucherid;
  const { maVoucher, phanTramGiamGia, ghiChu } = request.body;

  // B2: Kiểm tra dữ liệu
  if (!mongoose.Types.ObjectId.isValid(voucherId)) {
    return response.status(404).json({
      message: "Voucher ID Not Found",
    });
  }
  if (!maVoucher) {
    return response.status(400).json({
      status: "Bad Request",
      message: "Ma Voucher is required",
    });
  }
  if (phanTramGiamGia < 0 || !Number.isInteger(phanTramGiamGia)) {
    return response.status(400).json({
      status: "Bad Request",
      message: "Phan Tram Giam Gia is required",
    });
  }

  // B3: Xử lý Model
  try {
    const updateVoucher = {
      maVoucher: maVoucher,
      phanTramGiamGia: phanTramGiamGia,
      ghiChu: ghiChu,
    };

    const result = await voucherModel.findByIdAndUpdate(
      voucherId,
      updateVoucher
    );

    return response.status(200).json({
      status: "Update Success",
      data: result,
    });
  } catch (error) {
    return response.status(500).json({
      message: "Có lỗi",
    });
  }
};

const deleteVoucher = async (request, response) => {
  // B1: Thu thập dữ liệu
  const voucherId = request.params.voucherid;

  // B2: Kiểm tra dữ liệu
  if (!mongoose.Types.ObjectId.isValid(voucherId)) {
    return response.status(404).json({
      message: "Voucher ID Not Found",
    });
  }

  // B3: Xử lý Model
  try {
    const result = await voucherModel.findByIdAndDelete(voucherId);

    return response.status(200).json({
      message: "Delete success",
    });
  } catch (error) {
    return response.status(500).json({
      message: "Có lỗi",
    });
  }
};

module.exports = {
  getAllVouchers,
  getVoucherById,
  createVoucher,
  updateVoucher,
  deleteVoucher,
};
