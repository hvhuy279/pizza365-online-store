const mongoose = require("mongoose");
const orderModel = require("../models/order.model");
const userModel = require("../models/user.model");

const createOrder = async (request, response) => {
  // B1: Thu thập dữ liệu
  const { pizzaSize, pizzaType, voucher, drink, status, userId } = request.body;

  // B2: Kiểm tra dữ liệu
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return response.status(400).json({
      message: "User ID is invalid",
    });
  }
  if (!mongoose.Types.ObjectId.isValid(voucher)) {
    return response.status(400).json({
      message: "Voucher ID is invalid",
    });
  }
  if (!mongoose.Types.ObjectId.isValid(drink)) {
    return response.status(400).json({
      message: "Drink ID is invalid",
    });
  }

  // B3: Xử lý dữ liệu
  const newOrder = {
    pizzaSize: pizzaSize,
    pizzaType: pizzaType,
    voucher: voucher,
    drink: drink,
    status: status,
  };

  try {
    const createdOrder = await orderModel.create(newOrder);
    const updateUser = await userModel.findByIdAndUpdate(userId, {
      $push: { orders: createdOrder._id },
    });

    return response.status(201).json({
      status: "Create Order Success",
      data: createdOrder,
    });
  } catch (error) {
    return response.status(500).json({
      message: "Unexpected Error",
    });
  }
};

const getAllOrders = async (request, response) => {
  // B1: Thu thập dữ liệu
  const userId = request.query.userid;

  // B2: Kiểm tra dữ liệu
  if (userId !== undefined && !mongoose.Types.ObjectId.isValid(userId)) {
    return response.status(400).json({
      status: "Bad Request",
      message: "User ID is not valid",
    });
  }

  // B3: Xử lý model
  try {
    if (userId === undefined) {
      const result = await orderModel.find();

      if (result && result.length > 0) {
        return response.status(200).json({
          status: "Get all Orders success",
          data: result,
        });
      } else {
        return response.status(404).json({
          message: "Not Found any orders",
        });
      }
    } else {
      const userInfo = await userModel.findById(userId).populate("orders");

      return response.status(200).json({
        status: "Get all Orders of User success",
        data: userInfo.orders,
      });
    }
  } catch (error) {
    return response.status(500).json({
      status: "Internal Server Error",
      message: error.message,
    });
  }
};

const getOrderById = async (request, response) => {
  // B1: Thu thập dữ liệu
  const orderId = request.params.orderid;

  // B2: Kiểm tra dữ liệu
  if (!mongoose.Types.ObjectId.isValid(orderId)) {
    return response.status(400).json({
      status: "Bad Request",
      message: "Order ID is invalid",
    });
  }

  // B3: Xử lý Model
  try {
    const result = await orderModel.findById(orderId);

    if (result) {
      return response.status(200).json({
        message: "Get Order By ID success",
        data: result,
      });
    } else {
      return response.status(404).json({
        message: "Order ID Not Found",
      });
    }
  } catch (error) {
    return response.status(500).json({
      status: "Unexpected Error",
      message: error.message,
    });
  }
};

const updateOrderById = async (request, response) => {
  // B1: Thu thập dữ liệu
  const orderId = request.params.orderid;
  const { pizzaSize, pizzaType, voucher, drink, status } = request.body;

  // B2: Kiểm tra dữ liệu
  if (!mongoose.Types.ObjectId.isValid(orderId)) {
    return response.status(400).json({
      status: "Bad Request",
      message: "Order ID is invalid",
    });
  }

  // B3: Xử lý model
  try {
    const updateOrder = {
      pizzaSize: pizzaSize,
      pizzaType: pizzaType,
      voucher: voucher,
      drink: drink,
      status: status,
    };

    const result = await orderModel.findByIdAndUpdate(orderId, updateOrder);

    if (result) {
      return response.status(200).json({
        status: "Update Order Success",
        data: result,
      });
    } else {
      return response.status(404).json({
        status: "Order Not Found",
      });
    }
  } catch (error) {
    return response.status(500).json({
      status: "Unexpected Error",
      message: error.message,
    });
  }
};

const deleteOrderById = async (request, response) => {
  // B1: Thu thập dữ liệu
  const orderId = request.params.orderid;

  // Nếu muốn xoá ID thừa trong mảng User thì có thể truyền thêm userId để xoá
  const userId = request.query.userid;

  // B2: Kiểm tra dữ liệu
  if (!mongoose.Types.ObjectId.isValid(orderId)) {
    return response.status(400).json({
      status: "Bad Request",
      message: "Order ID is invalid",
    });
  }

  // B3: Xử lý Model
  try {
    const deleteOrder = await orderModel.findByIdAndDelete(orderId);

    // Nếu có user ID thì xoá thêm (optional)
    if (userId !== undefined) {
      await userModel.findByIdAndUpdate(userId, {
        $pull: { orders: orderId },
      });
    }

    if (deleteOrder) {
      return response.status(200).json({
        status: "Delete Order success",
        data: deleteOrder,
      });
    } else {
      return response.status(404).json({
        status: "Order ID Not Found",
      });
    }
  } catch (error) {
    return response.status(500).json({
      status: "Unexpected Error",
      message: error.message,
    });
  }
};

module.exports = {
  createOrder,
  getAllOrders,
  getOrderById,
  updateOrderById,
  deleteOrderById,
};
