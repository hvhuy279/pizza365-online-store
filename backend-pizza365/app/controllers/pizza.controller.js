const userModel = require("../models/user.model");
const orderModel = require("../models/order.model");
const drinkModel = require("../models/drink.model");
const mongoose = require("mongoose");

const createOrder = async (req, res) => {
  const { pizzaSize, pizzaType, voucher, drink, status, email, userId } =
    req.body;

  const vOrder = {
    pizzaSize,
    pizzaType,
    voucher,
    drink,
    status,
  };

  try {
    const checkUser = await userModel.findOne({
      email: email,
    });

    if (checkUser) {
      const result = await orderModel.create(vOrder);
      const updateUser = await userModel.findByIdAndUpdate(userId, {
        $push: { orders: result._id },
      });

      console.log("result", result);
      console.log("updateUser", updateUser);

      return res.status(201).json(result);
    } else {
      return res.status(404).json("Email User Not Found");
    }
  } catch (error) {
    return res.status(500).json("Lỗi Server");
  }
};

const getDrinkList = async (req, res) => {
  try {
    const result = await drinkModel.find();

    return res.status(200).json(result);
  } catch (error) {
    return res.status(500).json({
      status: "Internal Server Error",
      message: error.message,
    });
  }
};

module.exports = { createOrder, getDrinkList };
