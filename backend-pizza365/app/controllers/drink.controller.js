// Import Mongoose
const mongoose = require("mongoose");
// Import Models
const drinkModel = require("../models/drink.model");

const getAllDrinks = async (request, response) => {
  // B1: Thu thập dữ liệu
  // B2: Kiểm tra dữ liệu
  // B3: Thực hiện xử lý Model
  try {
    const result = await drinkModel.find();

    return response.status(200).json({
      status: "Get all Drinks success",
      data: result,
    });
  } catch (error) {
    return response.status(500).json({
      status: "Internal Server Error",
      message: error.message,
    });
  }
};

const getDrinkById = async (request, response) => {
  // B1: Thu thập dữ liệu
  const drinkId = request.params.drinkid;

  // B2: Kiểm tra dữ liệu
  if (!mongoose.Types.ObjectId.isValid(drinkId)) {
    return response.status(400).json({
      message: "Drink ID is invalid",
    });
  }

  // B3 : Thực hiện xử lý Model
  try {
    const result = await drinkModel.findById(drinkId);

    return response.status(200).json({
      message: "Get Course By ID success",
      data: result,
    });
  } catch (error) {
    return response.status(500).json({
      message: "Có lỗi",
    });
  }
};

const createDrinks = async (request, response) => {
  // B1: Thu thập dữ liệu
  const { maNuocUong, tenNuocUong, donGia } = request.body;

  //   B2: Kiểm tra dữ liệu
  if (!maNuocUong) {
    return response.status(400).json({
      status: "Bad Request",
      message: "Ma Nuoc Uong is required!",
    });
  }
  if (!tenNuocUong) {
    return response.status(400).json({
      status: "Bad Request",
      message: "Ten Nuoc Uong is required!",
    });
  }
  if (!Number.isInteger(donGia) || donGia < 0) {
    return response.status(400).json({
      status: "Bad Request",
      message: "Don gia is invalid!",
    });
  }

  // B3: Thực hiện thao tác Model
  try {
    const newDrink = {
      maNuocUong: maNuocUong,
      tenNuocUong: tenNuocUong,
      donGia: donGia,
    };

    const result = await drinkModel.create(newDrink);

    return response.status(201).json({
      status: "Create new Drink success",
      data: result,
    });
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập Error
    return response.status(500).json({
      status: "Internal Server Error",
      message: error.message,
    });
  }
};

const updateDrinkById = async (request, response) => {
  // B1: Thu thập dữ liệu
  const drinkId = request.params.drinkid;
  const { maNuocUong, tenNuocUong, donGia } = request.body;

  // B2: Kiểm tra dữ liệu
  if (!mongoose.Types.ObjectId.isValid(drinkId)) {
    return response.status(400).json({
      message: "Drink ID is invalid",
    });
  }
  if (!maNuocUong) {
    return response.status(400).json({
      status: "Bad Request",
      message: "Ma Nuoc Uong is required!",
    });
  }
  if (!tenNuocUong) {
    return response.status(400).json({
      status: "Bad Request",
      message: "Ten Nuoc Uong is required!",
    });
  }
  if (!Number.isInteger(donGia) || donGia < 0) {
    return response.status(400).json({
      status: "Bad Request",
      message: "Don gia is invalid!",
    });
  }

  // B3: Thực hiện xử lý Model
  try {
    const updateDrink = {
      maNuocUong: maNuocUong,
      tenNuocUong: tenNuocUong,
      donGia: donGia,
    };

    const result = await drinkModel.findByIdAndUpdate(drinkId, updateDrink);

    if (result) {
      return response.status(200).json({
        message: "Update Success",
        data: result,
      });
    } else {
      return response.status(404).json({
        message: "Drink Info Not Found",
      });
    }
  } catch (error) {
    return response.status(500).json({
      message: "Có lỗi",
    });
  }
};

const deleteDrinkById = async (request, response) => {
  // B1: Thu thập dữ liệu
  const drinkId = request.params.drinkid;

  // B2: Kiểm tra dữ liệu
  if (!mongoose.Types.ObjectId.isValid(drinkId)) {
    return response.status(400).json({
      message: "Drink ID is invalid",
    });
  }

  // B3: Thực hiện xử lý Model
  try {
    const result = await drinkModel.findByIdAndDelete(drinkId);

    if (result) {
      return response.status(200).json({
        message: "Delete Drink Success",
      });
    } else {
      return response.status(404).json({
        message: "Drink ID Not Found",
      });
    }
  } catch (error) {
    return response.status(500).json({
      message: "Có lỗi",
    });
  }
};

module.exports = {
  getAllDrinks,
  createDrinks,
  getDrinkById,
  updateDrinkById,
  deleteDrinkById,
};
