const getAllDrinksMiddleware = (request, response, next) => {
  console.log("Get all Drinks middleware");

  next();
};
const getDrinkByIdMiddleware = (request, response, next) => {
  console.log("Get Drinks by ID middleware");

  next();
};
const createDrinkMiddleware = (request, response, next) => {
  console.log("Create Drink middleware");

  next();
};
const updateDrinkByIdMiddleware = (request, response, next) => {
  console.log("Update Drink by ID middleware");

  next();
};
const deleteDrinkByIdMiddleware = (request, response, next) => {
  console.log("Delete Drink by ID middleware");

  next();
};

module.exports = {
  getAllDrinksMiddleware,
  getDrinkByIdMiddleware,
  createDrinkMiddleware,
  updateDrinkByIdMiddleware,
  deleteDrinkByIdMiddleware,
};
