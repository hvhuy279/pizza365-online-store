const getAllOrders = (request, response, next) => {
  console.log("Get all Orders");

  next();
};
const getOrderById = (request, response, next) => {
  console.log("Get Order by ID");

  next();
};
const createOrder = (request, response, next) => {
  console.log("Create Order");

  next();
};
const updateOrderById = (request, response, next) => {
  console.log("Update Order by ID");

  next();
};
const deleteOrderById = (request, response, next) => {
  console.log("Delete Order by ID");

  next();
};

module.exports = {
  getAllOrders,
  getOrderById,
  createOrder,
  updateOrderById,
  deleteOrderById,
};
