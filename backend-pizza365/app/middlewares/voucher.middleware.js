const getAllVouchersMiddleware = (request, response, next) => {
  console.log("Get all Vouchers");

  next();
};
const getVoucherByIdMiddleware = (request, response, next) => {
  console.log("Get Vouchers By ID");

  next();
};
const createVoucherMiddleware = (request, response, next) => {
  console.log("Create Vouchers");

  next();
};
const updateVoucherByIdMiddleware = (request, response, next) => {
  console.log("Update Vouchers");

  next();
};
const deleteVoucherByIdMiddleware = (request, response, next) => {
  console.log("Delete Vouchers");

  next();
};

module.exports = {
  getAllVouchersMiddleware,
  getVoucherByIdMiddleware,
  createVoucherMiddleware,
  updateVoucherByIdMiddleware,
  deleteVoucherByIdMiddleware,
};
