const getAllUsersMiddleware = (request, response, next) => {
  console.log("Get all Users");

  next();
};
const getUserByIdMiddleware = (request, response, next) => {
  console.log("Get User by ID");

  next();
};
const createUserMiddleware = (request, response, next) => {
  console.log("Create Users");

  next();
};
const updateUserByIdMiddleware = (request, response, next) => {
  console.log("Update User by ID");

  next();
};
const deleteUserByIdMiddleware = (request, response, next) => {
  console.log("Delete User by ID");

  next();
};

module.exports = {
  getAllUsersMiddleware,
  getUserByIdMiddleware,
  createUserMiddleware,
  updateUserByIdMiddleware,
  deleteUserByIdMiddleware,
};
