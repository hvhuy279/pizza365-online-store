// Import các thư viện
const express = require("express");
const path = require("path");
const mongoose = require("mongoose");

// Import các Router
const drinkRouter = require("./app/routes/drink.router");
const voucherRouter = require("./app/routes/voucher.router");
const userRouter = require("./app/routes/user.router");
const orderRouter = require("./app/routes/order.router");
const pizzaRouter = require("./app/routes/pizza.route");

// Khởi tạo app
const app = express();

// Khởi tạo cổng (port)
const port = 8000;

// Use Express JSON
app.use(express.json());

// Connect MongoDB
mongoose
  .connect("mongodb://127.0.0.1:27017/CRUD_Pizza365")
  .then(() => {
    console.log("Connect MongoDB successfully");
  })
  .catch((error) => {
    console.error(error);
    throw new error();
  });
// Chạy File trên ứng dụng Web
app.use(express.static(__dirname + "/views"));
// Khai báo API dạng GET "/" sẽ chạy vào đây
app.get("/", (request, response) => {
  response.sendFile(path.join(__dirname + "/views/pizza365index.html"));
});

// Use Router
app.use("/api/drinks", drinkRouter);
app.use("/api/vouchers", voucherRouter);
app.use("/api/users", userRouter);
app.use("/api/orders", orderRouter);
app.use("/devcamp-pizza365", pizzaRouter);

// Start App
app.listen(port, () => {
  console.log("App listening on port ", port);
});
