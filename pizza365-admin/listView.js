'use strict';
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gREQUEST_STATUS_OK = 200;
const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;
const gREQUEST_CREATE_SUCCESS = 201; // status 201 - tạo thành công
const gREQUEST_DELETE_SUCCESS = 204; // status 204 - Xóa thành công
const gBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";
const gEND_OF_COL = -1;

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm tải dữ liệu khi load trang
function onPageLoading() {
    getAllOrder();
}

// Hàm sự kiện lấy Info Order
function onBtnGetInfoOrderClick(paramOrder) {
    // Lấy dữ liệu ID và Order Code trong Dataset
    const vID = paramOrder.dataset.id;
    const vOrderCode = paramOrder.dataset.ordercode;

    // Hiển thị ID và Order Code ra console.log
    console.log(vID);
    console.log(vOrderCode);

    const vORDER_DETAIL_URL = "orderDetail.html";
    const vUrlSiteToOpen = vORDER_DETAIL_URL + "?" +
    "id=" + vID +
    "&orderCode=" + vOrderCode;

    window.location.href = vUrlSiteToOpen;
}

// Hàm sự kiện Delete Order
function onBtnDeleteOrderClick(paramOrder) {
    // Lấy dữ liệu ID và Order Code trong Dataset
    const vID = paramOrder.dataset.id;
    const vOrderCode = paramOrder.dataset.ordercode;

    // Gửi xác nhận người dùng có muốn xoá Order này không?
    const isConfirmDelete = confirm("Bạn có chắc chắn muốn xoá Order này?");
    
    // Nếu người dùng bấm OK
    if (isConfirmDelete) {
        const vLIST_VIEW_FORM = "indexListView.html"
        // Gọi server: Delete User By ID
        const vHttpDeleteUser = new XMLHttpRequest();

        vHttpDeleteUser.open("DELETE", gBASE_URL + "/" + vID, true);
        vHttpDeleteUser.send();

        vHttpDeleteUser.onreadystatechange = function() {
            if (this.status === gREQUEST_DELETE_SUCCESS && this.readyState === gREQUEST_READY_STATUS_FINISH_AND_OK) {
                alert("Đã xoá thành công");
                window.location.href = vLIST_VIEW_FORM;
            }
        }
    }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

// Hàm lấy hết dữ liệu trong server
function getAllOrder() {
    // Xử lý lấy dữ liệu trong server
    const vHttpGetAllOrder = new XMLHttpRequest();
    processGetAllOrder(vHttpGetAllOrder);
    vHttpGetAllOrder.onreadystatechange = function() {
        if (this.status === gREQUEST_STATUS_OK && this.readyState === gREQUEST_READY_STATUS_FINISH_AND_OK) {
            const vAllOrder = JSON.parse(this.responseText);
            // Hiển thị dữ liệu trong console.log
            console.log(vAllOrder);

            loadDataToTable(vAllOrder);

            addEventListenerClick();
        }
    }
}

// Hàm xử lý API: Get All Order
function processGetAllOrder(paramRequest) {
    paramRequest.open("GET", gBASE_URL, true);
    paramRequest.send();
}

// Hàm load dữ liệu vào Table
function loadDataToTable(paramAllOrder) {
    // Truy xuất vào thẻ Table
    const vTable = document.getElementById("table-orders");
    const vTableBody = vTable.getElementsByTagName("tbody")[0];

    for (let bIndex = 0; bIndex < paramAllOrder.length; bIndex++) {
        // Thêm 1 Row mới
        const vNewRow = vTableBody.insertRow(gEND_OF_COL);

        // Thêm các Cell vào Row
        const vOrderCodeCell = vNewRow.insertCell(0);
        const vSizeComboCell = vNewRow.insertCell(1);
        const vPizzaTypeCell = vNewRow.insertCell(2);
        const vDrinkCell = vNewRow.insertCell(3);
        const vPriceCell = vNewRow.insertCell(4);
        const vNameCell = vNewRow.insertCell(5);
        const vPhoneCell = vNewRow.insertCell(6);
        const vStatusCell = vNewRow.insertCell(7);
        const vActionsCell = vNewRow.insertCell(8);

        // Điền dữ liệu vào các Cell
        vOrderCodeCell.innerHTML = paramAllOrder[bIndex].orderCode;
        vSizeComboCell.innerHTML = paramAllOrder[bIndex].kichCo;
        vPizzaTypeCell.innerHTML = paramAllOrder[bIndex].loaiPizza;
        vDrinkCell.innerHTML = paramAllOrder[bIndex].idLoaiNuocUong;
        vPriceCell.innerHTML = paramAllOrder[bIndex].thanhTien;
        vNameCell.innerHTML = paramAllOrder[bIndex].hoTen;
        vPhoneCell.innerHTML = paramAllOrder[bIndex].soDienThoai;
        vStatusCell.innerHTML = paramAllOrder[bIndex].trangThai;

        // Tạo Button "Chi Tiết"
        const vDetailButton = document.createElement("button");
        vDetailButton.innerHTML = "Chi Tiết";
        vDetailButton.className = "btn btn-info info-order";
        vDetailButton.setAttribute("data-id", paramAllOrder[bIndex].id);
        vDetailButton.setAttribute("data-ordercode", paramAllOrder[bIndex].orderCode);

        // Tạo Button "Delete"
        const vDeleteButton = document.createElement("button");
        vDeleteButton.innerHTML = "Delete";
        vDeleteButton.style.marginLeft = "0.5rem";
        vDeleteButton.className = "btn btn-danger order-delete";
        vDeleteButton.setAttribute("data-id", paramAllOrder[bIndex].id);
        vDeleteButton.setAttribute("data-ordercode", paramAllOrder[bIndex].orderCode);

        // Thêm Button "Chi Tiết" và "Delete" vào Actions Cell
        vActionsCell.appendChild(vDetailButton);
        vActionsCell.appendChild(vDeleteButton);
    }
}

// Hàm thêm sự kiện onclick cho Button
function addEventListenerClick() {
    // Truy xuất vào các thẻ button "Chi Tiết" và "Delete"
    const vListDetailButton = document.getElementsByClassName("info-order");
    const vListDeleteButton = document.getElementsByClassName("order-delete");

    // Gán sự kiện onclick vào Button "Chi Tiết"
    for (let bIndex = 0; bIndex < vListDetailButton.length; bIndex++) {
        vListDetailButton[bIndex].addEventListener("click", function() {
            onBtnGetInfoOrderClick(this);
        })
    }

    // Gán sự kiện onlick vào Button "Delete"
    for (let bIndex = 0; bIndex < vListDeleteButton.length; bIndex++) {
        vListDeleteButton[bIndex].addEventListener("click", function() {
            onBtnDeleteOrderClick(this);
        })
    }
}

// Cấu hình Button "Thêm đơn hàng"
function onBtnCreateOrderClick() {
    window.location.href = "create.order.html";
}