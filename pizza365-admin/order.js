'use strict';
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gREQUEST_STATUS_OK = 200;
const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;
const gREQUEST_CREATE_SUCCESS = 201; // status 201 - tạo thành công
const gREQUEST_DELETE_SUCCESS = 204; // status 204 - Xóa thành công
const gBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";
const gDRINK_URL = "http://203.171.20.210:8080/devcamp-pizza365/drinks";
const gEND_OF_COL = -1;

var gID;
var gOrderCode;

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
const vBtnUpdate = document.getElementById("btn-confirm");
vBtnUpdate.addEventListener("click", function() {
    // Ghi ID của Order ra console.log
    console.log(gID);

    // Khởi tạo đối tượng thay đổi trạng thái: confirmed
    const vOrderObjRequest = {
        trangThai: "confirmed",
    };

    // Gọi server để update "Confirmed" Order
    const vHttpUpdateOrder = new XMLHttpRequest();

    vHttpUpdateOrder.open("PUT", gBASE_URL + "/" + gID, true);
    vHttpUpdateOrder.setRequestHeader("Content-Type", "application/json");
    vHttpUpdateOrder.send(JSON.stringify(vOrderObjRequest));
    
    vHttpUpdateOrder.onreadystatechange = function() {
        if (this.status === gREQUEST_STATUS_OK && this.readyState === gREQUEST_READY_STATUS_FINISH_AND_OK) {
            const vUser = JSON.parse(this.responseText);

            // Hiển thị kết quả
            console.log(vUser);
        }
    }

});

const vBtnCancel = document.getElementById("btn-cancel");
vBtnCancel.addEventListener("click", function() {

    // Hiển thị ID của Order
    console.log(gID);

    // Khởi tạo đối tượng thay đổi trạng thái: cancel
    const vOrderObjRequest = {
        trangThai: "cancel",
    };

    // Gọi server để update "Cancel" Order
    const vHttpUpdateOrder = new XMLHttpRequest();
    
    vHttpUpdateOrder.open("PUT", gBASE_URL + "/" + gID, true);
    vHttpUpdateOrder.setRequestHeader("Content-Type", "application/json");
    vHttpUpdateOrder.send(JSON.stringify(vOrderObjRequest));

    vHttpUpdateOrder.onreadystatechange = function() {
        if (this.status === gREQUEST_STATUS_OK && this.readyState === gREQUEST_READY_STATUS_FINISH_AND_OK) {
            const vUser = JSON.parse(this.responseText);
            console.log(vUser);
        }
    }
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm tải dữ liệu khi load trang
function onPageLoading() {
    //  Truy xuất vào đường dẫn trang
    const vUrlString = window.location.href;
    const vUrl = new URL(vUrlString);

    // Thu thập ID và OrderCode
    gID = vUrl.searchParams.get("id");
    gOrderCode = vUrl.searchParams.get("orderCode");

    const vLIST_ORDER_FORM = "indexListView.html";

    // Kiểm tra ID và Order Code có ở QueryString hay không
    if (gID == null || gID == "") {
        alert("Không có ID của Order");
        window.location.href = vLIST_ORDER_FORM;
    }
    else if (gOrderCode == null || gOrderCode == "") {
        alert("Không có orderCode của Order");
        window.location.href = vLIST_ORDER_FORM;    
    }

    // Hiển thị ID và OrderCode vào console.log
    console.log(gID);
    console.log(gOrderCode);

    // Load dữ liệu vào Select Drink
    loadDataDrinkList();

    // Thực hiện API lấy dữ liệu (dựa vào Order Code)
    const vHttpGetOrder = new XMLHttpRequest();
    processGetOrder(vHttpGetOrder, gOrderCode);

    vHttpGetOrder.onreadystatechange = function() {
        if (this.status === gREQUEST_STATUS_OK && this.readyState === gREQUEST_READY_STATUS_FINISH_AND_OK) {
            const vOrderDetail = JSON.parse(this.responseText);
            // Hiển thị dữ liệu Order Detail ra console.log
            console.log(vOrderDetail);

            loadOrderDataToForm(vOrderDetail);
        }
    }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// Hàm xử lý API: Get Order By Order Code
function processGetOrder(paramRequest, paramOrderCode) {
    paramRequest.open("GET", gBASE_URL + "/" + paramOrderCode, true);
    paramRequest.send();
}

// Hàm load dữ liệu Drink và Select Drink
function loadDataDrinkList() {
    // Gọi server
    const vHttpGetDrink = new XMLHttpRequest();
    vHttpGetDrink.open("GET", gDRINK_URL, true);
    vHttpGetDrink.send();
    vHttpGetDrink.onreadystatechange = function() {
        if (this.status === gREQUEST_STATUS_OK && this.readyState === gREQUEST_READY_STATUS_FINISH_AND_OK) {
            // Hiển thị dữ liệu Drink List ra console.log
            console.log(this.responseText);
            const vDrinkList = JSON.parse(this.responseText);

            // Xử lý add dữ liệu Drink List vào Select Drink
            handleDrinkList(vDrinkList);
        }
    }
}

// Hàm load dữ liệu Drink List và Select Drink
function handleDrinkList(paramDrinkList) {
    const vSelectDrink = document.getElementById("select-drink");
    
    for (let bIndex = 0; bIndex < paramDrinkList.length; bIndex++) {
        const vOptionDrink = document.createElement("option");
        vOptionDrink.value = paramDrinkList[bIndex].maNuocUong;
        vOptionDrink.text = paramDrinkList[bIndex].tenNuocUong;
        vSelectDrink.appendChild(vOptionDrink);
    }
}

// Hàm load dữ liệu Order Detail ra Table
function loadOrderDataToForm(paramOrderObj) {
    // Truy xuất vào các thẻ input
    const vInputOrderCode = document.getElementById("input-order-code");
    const vSelectSizeCombo = document.getElementById("select-size-combo");
    const vDuongKinhPizza = document.getElementById("input-duong-kinh");
    const vSuonNuong = document.getElementById("input-suon-nuong");
    const vSelectDrink = document.getElementById("select-drink");
    const vInputNumberDrink = document.getElementById("input-number");
    const vInputVoucher = document.getElementById("input-voucher");
    const vSelectPizzaType = document.getElementById("select-pizza-type");
    const vInputSalad = document.getElementById("input-salad");
    const vInputPrice = document.getElementById("input-price");
    const vInputGiamGia = document.getElementById("input-giam-gia");
    const vInputFullName = document.getElementById("input-fullname");
    const vInputEmail = document.getElementById("input-email");
    const vInputPhone = document.getElementById("input-phone");
    const vInputAddress = document.getElementById("input-address");
    const vInputMessage = document.getElementById("input-message");
    const vSelectStatus = document.getElementById("select-status");
    const vInputCreateOrder = document.getElementById("input-create-order");
    const vInputUpdate = document.getElementById("input-update");

    // Điền dữ liệu vào các thẻ input
    vInputOrderCode.value = paramOrderObj.orderCode;
    vSelectSizeCombo.value = paramOrderObj.kichCo;
    vDuongKinhPizza.value = paramOrderObj.duongKinh;
    vSuonNuong.value = paramOrderObj.suon;
    vSelectDrink.value = paramOrderObj.idLoaiNuocUong;
    vInputNumberDrink.value = paramOrderObj.soLuongNuoc;
    vInputVoucher.value = paramOrderObj.idVourcher;
    vSelectPizzaType.value = paramOrderObj.loaiPizza;
    vInputSalad.value = paramOrderObj.salad;
    vInputPrice.value = paramOrderObj.thanhTien;
    vInputGiamGia.value = paramOrderObj.giamGia;
    vInputFullName.value = paramOrderObj.hoTen;
    vInputEmail.value = paramOrderObj.email;
    vInputPhone.value = paramOrderObj.soDienThoai;
    vInputAddress.value = paramOrderObj.diaChi;
    vInputMessage.value = paramOrderObj.loiNhan;
    vSelectStatus.value = paramOrderObj.trangThai;
    vInputCreateOrder.value = paramOrderObj.ngayTao;
    vInputUpdate.value = paramOrderObj.ngayCapNhat;
}